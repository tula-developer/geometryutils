﻿namespace GeometryUtils {
    public class Circle : GeometryBase {
        public double Radius { get; }

        public Circle(double radius) {
            if (radius < 0)
                throw new ArgumentException("Circle can't be created with a negative radius.");
            
            Radius = radius;
        }
        protected override double CalculateArea() {
            return Math.PI * Radius * Radius;
        }
    }
}