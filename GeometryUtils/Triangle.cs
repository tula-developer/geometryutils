﻿namespace GeometryUtils {
    public class Triangle : GeometryBase {
        Lazy<bool> rightAngled;
        
        public double Side1 { get; }
        public double Side2 { get; }
        public double Side3 { get; }
        public bool IsRightAngled => rightAngled.Value;

        public Triangle(double side1, double side2, double side3) {
            if (side1 < 0 || side2 < 0 || side3 < 0)
                throw new ArgumentException("Triangle can't be created with a negative side length.");
            if (!IsValidSides(side1, side2, side3))
                throw new InvalidOperationException("Triangle can't be created with specified sides.");

            Side1 = side1;
            Side2 = side2;
            Side3 = side3;
            rightAngled = new Lazy<bool>(CheckIsRightAngled);
        }
        bool IsValidSides(double side1, double side2, double side3) {
            return side1 == 0 && side2 == 0 && side3 == 0 ||
                side1 + side2 > side3 && 
                side1 + side3 > side2 && 
                side2 + side3 > side1;
        }
        bool CheckIsRightAngled() {
            double maxSide = Math.Max(Side1, Math.Max(Side2, Side3));
            return 2 * maxSide * maxSide == Side1 * Side1 + Side2 * Side2 + Side3 * Side3;
        }
        protected override double CalculateArea() {
            double semiPerimeter = (Side1 + Side2 + Side3) / 2;
            return Math.Sqrt(semiPerimeter * (semiPerimeter - Side1) * (semiPerimeter - Side2) * (semiPerimeter - Side3));
        }
    }
}