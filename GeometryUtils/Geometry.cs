﻿namespace GeometryUtils {
    public interface IGeometry {
        public double GetArea();
    }

    public abstract class GeometryBase : IGeometry {
        Lazy<double> area;

        protected GeometryBase() {
            area = new Lazy<double>(CalculateArea);
        }
        protected abstract double CalculateArea();
        public double GetArea() => area.Value;
    }
}