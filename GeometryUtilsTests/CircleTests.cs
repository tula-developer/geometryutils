using GeometryUtils;
using NUnit.Framework;
using System;

namespace GeometryUtilsTests {
    [TestFixture]
    public class CircleGeometryTests : GeometryTestsBase {
        [Test]
        public void NegativeRadius() {
            Assert.Throws<ArgumentException>(() => new Circle(-1), "Expected ArgumentException");
        }
        [Test]
        public void AreaWithZeroRadius() {
            Circle circle = new Circle(0);
            Assert.AreEqual(0, circle.Radius);
            AssertGeometryArea(circle, 0);
        }
        [Test]
        public void AreaWithPositiveRadius() {
            Circle circle = new Circle(5);
            Assert.AreEqual(5, circle.Radius);
            AssertGeometryArea(circle, Math.PI * 25);
        }
    }
}