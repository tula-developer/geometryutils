﻿using GeometryUtils;
using NUnit.Framework;

namespace GeometryUtilsTests {
    public abstract class GeometryTestsBase {
        protected void AssertGeometryArea(IGeometry geometry, double expectedValue) { 
            Assert.AreEqual(expectedValue, geometry.GetArea(), double.Epsilon);
        }
    }
}