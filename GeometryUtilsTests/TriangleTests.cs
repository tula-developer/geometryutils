﻿using GeometryUtils;
using NUnit.Framework;
using System;

namespace GeometryUtilsTests {
    [TestFixture]
    public class TriangleGeometryTests : GeometryTestsBase {
        [Test]
        public void NegativeSide() {
            Assert.Throws<ArgumentException>(() => new Triangle(-1, 0, 5), "Expected ArgumentException on first side");
            Assert.Throws<ArgumentException>(() => new Triangle(0, -1, 5), "Expected ArgumentException on second side");
            Assert.Throws<ArgumentException>(() => new Triangle(0, 5, -1), "Expected ArgumentException on third side");
        }
        [Test]
        public void AreaWithZeroSide() {
            Triangle triangle = new Triangle(0, 0, 0);
            Assert.AreEqual(0, triangle.Side1);
            Assert.AreEqual(0, triangle.Side2);
            Assert.AreEqual(0, triangle.Side3);
            AssertGeometryArea(triangle, 0);
        }
        [Test]
        public void AreaWithPositiveSide() {
            Triangle triangle = new Triangle(3, 3, 3);
            Assert.AreEqual(3, triangle.Side1);
            Assert.AreEqual(3, triangle.Side2);
            Assert.AreEqual(3, triangle.Side3);
            AssertGeometryArea(triangle, Math.Sqrt(4.5 * 1.5 * 1.5 * 1.5));
        }
        [Test]
        public void RightAngeled() {
            Assert.IsFalse(new Triangle(1, 1, 1).IsRightAngled);
            Assert.IsFalse(new Triangle(6, 2, 5).IsRightAngled);
            Assert.IsTrue(new Triangle(3, 4, 5).IsRightAngled);
            Assert.IsTrue(new Triangle(4, 5, 3).IsRightAngled);
            Assert.IsTrue(new Triangle(5, 3, 4).IsRightAngled);
            Assert.IsTrue(new Triangle(5, 4, 3).IsRightAngled);
        }
    }
}